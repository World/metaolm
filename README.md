# Metaolm

*Unfortunately this project was abandoned.* For easy Matrix protocol E2EE, I would recommend putting your hope and work into [matrix-sdk](https://crates.io/crates/matrix-sdk).
In case you want to see this project revived, and are willing to put in the work to make it happen, make yourself heard in the Matrix channel linked below.

This is a monolithic software module for enabling a client
of the Matrix protocol with end-to-end encrypted communication.

Module and client communicate with each other over a bi-directional
communication channel.

This module is currently still unfinished and probably not very useful for client developers.

Matrix room for discussion: *[#olm-rs:matrix.org](https://matrix.to/#/#olm-rs:matrix.org)*

### Roadmap

Here is a loose rundown of what has yet to be done. This list is not complete and only
covers things I deem to be most noteworthy. While this should give you a rough overview of
the order in which things will be done, it doesn't automatically follow that we can't work
on some of these things simultaneously.

* [x] Basic support for decrypting Olm and Megolm events
* [x] ~~Refactor module to make use of async Rust~~ (see #25)
* [ ] Basic support for encrypting Olm and Megolm events (#22)
* [ ] Finalise documentation
* [ ] Integration with [Fractal](https://www.matrix.org/docs/projects/client/fractal.html) (and fix lots of bugs)
* [ ] Make Metaolm usable outside of Rustland

### Contributing
If you are considering to contribute, take a look at the CONTRIBUTING guide.

### Licensing
This project is licensed under the GPLv3+ license - for further information see the LICENSE file.
