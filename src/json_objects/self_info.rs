// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde_derive::{Deserialize, Serialize};

use super::{
    device::DeviceKeys, event::MegolmEncrypted, storage::OutboundGroupSessionWithInfoForStorage,
    transport::C2MEncryptEventBody,
};
use crate::session_cache::InboundMegolmSessionIdentifier;

#[derive(Serialize, Deserialize)]
pub struct RoomKeyBody {
    pub identifier: InboundMegolmSessionIdentifier,
    pub session_key: String,
    pub ed25519: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MegolmEncryptedBody {
    pub event: MegolmEncrypted,
    pub blocker_id: u32,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmEncryptBody {
    pub session: OutboundGroupSessionWithInfoForStorage,
    pub room_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmEncryptClaimOTKBody {
    pub event_to_encrypt: C2MEncryptEventBody,
    pub device_keys: DeviceKeys,
}
