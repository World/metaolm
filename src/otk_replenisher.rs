// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::time::{Duration, Instant};

use crate::{
    endpoints,
    json_objects::otk::{AccountOneTimeKeys, OneTimeKeys, SignedOneTimeKey},
    packet::{HTTPReqType, OutPacket, OutPacketType},
};

use olm_rs::account::OlmAccount;
use serde_json;

/// An instance of OTKReplenisher will keeps track of when
/// you sent the last request for replenishing one-time-keys
/// on the Matrix server and can tell you when another one
/// should be sent.
pub(crate) struct OTKReplenisher {
    /// The duration after which the Matrix server should be asked
    /// if new one-time-keys need to be published.
    /// The Matrix documentation recommends 10 minutes.
    replenish_cycle: Duration,
    last_checked: Instant,
    replenish_goal: usize,
}

impl OTKReplenisher {
    /// Creates a new OTKReplenisher.
    /// replenish_goal refers to the amount of one time keys
    /// that should be maintained on the Matrix server.
    ///
    /// # Parameters
    /// * `replenish_goal` amount of one time keys that should
    /// be maintained on the Matrix server
    pub fn new(replenish_goal: usize) -> Self {
        OTKReplenisher {
            // replenish cycle of 10 minutes; hard-coded for now
            replenish_cycle: Duration::from_secs(60 * 60 * 10),
            last_checked: Instant::now(),
            replenish_goal,
        }
    }

    /// Tells you if a refresh on checking on the amount of
    /// one-time-keys on the server is due.
    /// A cycle is roughly 10 minutes as of now.
    pub fn replenish_due(&self) -> bool {
        self.last_checked.elapsed() > self.replenish_cycle
    }

    /// Creates the communication object for uploading new
    /// one time keys if necessary. Necessity is determined by checking
    /// if `otk_count` exceeds the set `replenish_goal`.
    ///
    /// # Parameters
    /// * `otk_count` amount of one-time-keys currently stored
    /// on the Matrix server
    pub fn replenish_for(
        &mut self,
        otk_count: usize,
        olm_account: &OlmAccount,
        device_id: &str,
        user_id: &str,
        blocker_id: u32,
    ) -> Option<OutPacket> {
        if otk_count >= self.replenish_goal {
            None
        } else {
            // reset counter
            self.last_checked = Instant::now();

            // create amount of OTKs to satisfy the replenish goal
            olm_account.generate_one_time_keys(self.replenish_goal - otk_count);
            let otks_json: AccountOneTimeKeys =
                serde_json::from_str(&olm_account.one_time_keys()).unwrap();
            // get the direct mapping of OTK ID to OTK value
            let otk_map = otks_json.curve25519;

            // construct final query object
            let mut signed_otks = OneTimeKeys::new();
            for (index, one_time_key) in otk_map.iter() {
                let mut key = SignedOneTimeKey::new(one_time_key.as_str());
                let key_signature =
                    olm_account.sign(&serde_json::to_string(&key.get_otk_only()).unwrap());
                key.set_signature(user_id, device_id, &key_signature);
                signed_otks.add_key(index, key);
            }

            Some(OutPacket {
                header: OutPacketType::HTTPReq(
                    HTTPReqType::Post,
                    endpoints::UPLOAD_KEYS.to_string(),
                ),
                body: serde_json::to_string(&signed_otks).unwrap(),
                blocker_id: Some(blocker_id),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::json_objects::olm::OlmIdentityKeys;
    use olm_rs::utility::OlmUtility;

    #[test]
    fn replenish_signal_valid() {
        let account = OlmAccount::new();
        let replenish_goal = account.max_number_of_one_time_keys() / 2;
        let mut replenisher = OTKReplenisher::new(replenish_goal);

        // replenisher was just initialised
        assert!(!replenisher.replenish_due());

        let otk_count = replenish_goal - 3;

        let replenish_signal = replenisher
            .replenish_for(otk_count, &account, "device_id", "user@example.org", 0)
            .unwrap();

        let replenish_signal_json: serde_json::Value =
            serde_json::from_str(&replenish_signal.body).unwrap();
        let replenish_signal_map = replenish_signal_json.as_object().unwrap();
        assert_eq!(1, replenish_signal_map.len());

        let one_time_keys = replenish_signal_map.get("one_time_keys").unwrap();
        let one_time_keys_map = one_time_keys.as_object().unwrap();
        assert_eq!(3, one_time_keys_map.len());

        let public_key_pair: OlmIdentityKeys =
            serde_json::from_str(&account.identity_keys()).unwrap();
        let sign_key = public_key_pair.ed25519;
        let utility = OlmUtility::new();

        // test signature for each OTK
        for key_json in one_time_keys_map.values() {
            let key: SignedOneTimeKey = serde_json::from_str(&key_json.to_string()).unwrap();
            let signature = key.signatures.values().next().unwrap();
            let mut signature_str = signature.signatures.values().next().unwrap().clone();
            assert!(utility
                .ed25519_verify(
                    &sign_key,
                    &serde_json::to_string(&key.get_otk_only()).unwrap(),
                    &mut signature_str
                )
                .unwrap());
        }
    }
}
