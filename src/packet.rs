// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    endpoints,
    json_objects::{
        event::OlmEncrypted,
        self_info::{MegolmEncryptClaimOTKBody, MegolmEncryptedBody, RoomKeyBody},
        transport::C2MEncryptEventBody,
    },
};

/// Object used for the client to communicate with
/// the Metaolm module, but not the other way around -
/// that's what `OutPacket` is for.
pub struct InPacket {
    pub header: InPacketType,
    pub body: String,
    pub blocker_id: Option<u32>,
}

/// Object used for the Metaolm module to communicate with
/// the client, but now the other way around -
/// that's what `InPacket` is for.
pub struct OutPacket {
    pub header: OutPacketType,
    pub body: String,
    pub blocker_id: Option<u32>,
}

pub(crate) enum Blocker {
    /// Blocker for sending one-time-key count request to the Matrix server and uploading
    /// new keys if needed on receiving the response.
    DoOTKReplenish,
    /// Blocker for publishing new one-time-keys to the Matrix server and marking them as
    /// published on response.
    OTKListPublished,
    /// Used for the callback when claiming one-time-keys for establishing Olm sessions
    /// with other devices.
    ClaimOTK(MegolmEncryptClaimOTKBody, u32),
    /// Used as a callback, when an `OlmSession` is required for decrypting, but not
    /// in Metaolm's cache and it has to be checked if the client has the session stored.
    OlmDecrypt(OlmEncrypted, u32),
    /// Used as a callback, when a `MegolmSession` is required for encrypting, but not
    /// in Metaolm's cache and it has to be checked if the client has the session stored.
    MegolmEncrypt(C2MEncryptEventBody, u32),
    /// Used as a callback, when a `MegolmSession` is required for decrypting, but not
    /// in Metaolm's cache and it has to be checked if the client has the session stored.
    MegolmDecrypt(MegolmEncryptedBody),
    /// When receiving an `m.room_key` event and there is no cached Megolm session
    /// associated with the key, we ask for a potential previously stored session.
    /// When no session is stored, we have to create the new session from the info contained
    /// within this item.
    RoomKey(RoomKeyBody),
    /// Callback for when the client answers with the users list for the requested room,
    /// contains the `blocker_id` associated with the following `RequestedDeviceKeys` item.
    RequestedUsersForRoom(u32),
    /// The received device keys are used for establishing a new Megolm session. For this
    /// the contained items are needed, first the original event that is to be encrypted,
    /// and the `blocker_id` tied to it.
    RequestedDeviceKeys(C2MEncryptEventBody, u32),
}

#[derive(Debug, PartialEq)]
pub enum InPacketType {
    ActionReq(ActionType),
    HTTPRsp(HTTPReqType, String),
    ProvideInfo, // TODO: flesh out with enum
    StorageRsp,
    TerminateModule,
}

#[derive(Debug, PartialEq)]
pub enum OutPacketType {
    ActionRsp(ActionType),
    HTTPReq(HTTPReqType, String),
    Load(StorageDesc),
    ModuleError(String),
    RequestInfo(ModuleInfoReq),
    Store(StorageDesc),
}

#[derive(Debug, PartialEq)]
pub enum ModuleInfoReq {
    UsersForRoom { room_id: String },
}

#[derive(Debug, PartialEq)]
pub enum StorageDesc {
    OlmSession {
        device_id: String,
        sender_key: String,
    },
    Account {
        device_id: String,
    },
    InboundMegolmSession {
        room_id: String,
        sender_key: String,
        session_id: String,
    },
    OutboundMegolmSession {
        room_id: String,
    },
}

#[derive(Debug, PartialEq)]
pub enum HTTPReqType {
    Post,
    Put,
    Get,
}

#[derive(Debug, PartialEq)]
pub enum ActionType {
    EnableEncryption,
    ForwardEncryptedEvent,
    EncryptEvent,
}

impl OutPacket {
    /// Constructs a packet for getting the amount
    /// of one time keys from the Matrix server
    pub fn get_otk_count_req(blocker_id: u32) -> Self {
        OutPacket {
            header: OutPacketType::HTTPReq(HTTPReqType::Post, endpoints::UPLOAD_KEYS.to_string()),
            body: String::from("{}"),
            blocker_id: Some(blocker_id),
        }
    }

    /// Constructs a ModuleError packet
    pub fn new_module_error(error_type: &str, msg: &str) -> Self {
        OutPacket {
            header: OutPacketType::ModuleError(error_type.to_string()),
            body: String::from(msg),
            blocker_id: None,
        }
    }
}
