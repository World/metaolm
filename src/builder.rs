// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::blocker::{BlockerManager, Participant};
use crate::endpoints;
use crate::json_objects::{device::SignedDeviceKeys, olm::OlmIdentityKeys};
use crate::metaolm::Metaolm;
use crate::packet::*;
use crate::session_cache::SessionCache;
use olm_rs::{account::OlmAccount, errors::OlmAccountError, PicklingMode};
use serde_json;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::sync::mpsc::{Receiver, Sender};

/// Used to build a Metaolm object.
pub struct MetaolmBuilder {
    device_id: Option<String>,
    user_id: Option<String>,
    olm_account: OlmAccount,
    blocker_manager: BlockerManager,
    blocked_items: BTreeMap<u32, Blocker>,
    sender: Option<Sender<OutPacket>>,
    receiver: Option<Receiver<InPacket>>,
}

impl MetaolmBuilder {
    /// Creates a new instance of MetaolmBuilder and initialises
    /// all necessary values.
    pub fn new() -> Self {
        let account = OlmAccount::new();

        MetaolmBuilder {
            device_id: None,
            user_id: None,
            olm_account: account,
            blocker_manager: BlockerManager::new(Participant::Metaolm),
            blocked_items: BTreeMap::new(),
            sender: None,
            receiver: None,
        }
    }

    /// Sets the device ID for the module. Is required.
    pub fn with_device_id(mut self, device_id: &str) -> MetaolmBuilder {
        self.device_id = Some(String::from(device_id));

        self
    }

    /// Sets the user ID for the module. Is required.
    pub fn with_user_id(mut self, user_id: &str) -> MetaolmBuilder {
        self.user_id = Some(String::from(user_id));

        self
    }

    /// Sets the communication channels for communication with the client.
    ///
    /// # Parameters
    /// * sender: Sender used for sending data to the client
    /// * receiver: Receiver used for receiving data sent from the client
    pub fn connected(
        mut self,
        sender: Sender<OutPacket>,
        receiver: Receiver<InPacket>,
    ) -> MetaolmBuilder {
        self.sender = Some(sender);
        self.receiver = Some(receiver);

        self
    }

    /// Finishes the building process for an instance of Metaolm.
    /// Requires that all required functions have been called.
    pub fn finish<'a>(self) -> Result<Metaolm, Cow<'a, str>> {
        let mut finish_blocker = None;

        if self.device_id.is_none() {
            finish_blocker = Some("device_id");
        } else if self.user_id.is_none() {
            finish_blocker = Some("user_id");
        } else if self.sender.is_none() {
            finish_blocker = Some("sender");
        } else if self.receiver.is_none() {
            finish_blocker = Some("receiver");
        }

        if let Some(x) = finish_blocker {
            return Err(format!(
                "{} has to be set for Metaolm, before finishing the building stage!",
                x
            )
            .into());
        };

        let mut metaolm = Metaolm {
            device_id: self.device_id.unwrap(),
            user_id: self.user_id.unwrap(),
            olm_account: self.olm_account,
            blocker_manager: self.blocker_manager,
            blocked_items: self.blocked_items,
            session_cache: SessionCache::new(),
            sender: self.sender.unwrap(),
            receiver: self.receiver.unwrap(),
        };

        let device_req_id = metaolm.blocker_manager.next_blocker_id();

        // request for getting potentailly stored OlmAccount
        let device_req = OutPacket {
            header: OutPacketType::Load(StorageDesc::Account {
                device_id: metaolm.device_id.clone(),
            }),
            body: String::new(),
            blocker_id: Some(device_req_id),
        };

        metaolm.send(device_req);

        // waiting for client response
        let mut answer;
        loop {
            answer = metaolm.receiver.recv().unwrap();

            if let Some(id) = answer.blocker_id {
                if id == device_req_id {
                    break;
                }
            }
        }

        // OlmAccount for the specified device ID is stored and
        // is contained in the response
        if answer.body != String::new() {
            let stored_account = OlmAccount::unpickle(answer.body, PicklingMode::Unencrypted);

            match stored_account {
                Ok(account) => {
                    metaolm.olm_account = account;
                }
                Err(OlmAccountError::InvalidBase64) => {
                    return Err("The given account isn't valid base64 coding!".into());
                }
                Err(OlmAccountError::BadAccountKey) => {
                    return Err("The given account was encrypted, which is not implemented".into());
                }
                Err(_) => unreachable!(),
            }
        }
        // empty response body means that no OlmAccount for this device id
        // is stored, so we have to tell the client to store the one
        // we already created
        else {
            let store_request_blocker = metaolm.blocker_manager.next_blocker_id();
            let register_request_blocker = metaolm.blocker_manager.next_blocker_id();

            // build a store request for the client
            let store_request = OutPacket {
                header: OutPacketType::Store(StorageDesc::Account {
                    device_id: metaolm.device_id.clone(),
                }),
                body: metaolm.olm_account.pickle(PicklingMode::Unencrypted),
                blocker_id: Some(store_request_blocker),
            };

            // and send it
            metaolm.send(store_request);

            // build the register request for the Matrix server
            let identity_keys: OlmIdentityKeys =
                serde_json::from_str(&metaolm.olm_account.identity_keys()).unwrap();
            let mut device_keys =
                SignedDeviceKeys::new(&metaolm.device_id, &metaolm.user_id, identity_keys);

            // dump constructed object as Canonical JSON
            let device_keys_signature = metaolm
                .olm_account
                .sign(&serde_json::to_string(&device_keys.get_device_keys_only()).unwrap());

            device_keys.set_signature(&device_keys_signature);

            // build the register request for the client that is to be
            // forwarded to the Matrix server
            let register_request = OutPacket {
                header: OutPacketType::HTTPReq(
                    HTTPReqType::Post,
                    endpoints::UPLOAD_KEYS.to_string(),
                ),
                body: serde_json::to_string(&device_keys).unwrap(),
                blocker_id: Some(register_request_blocker),
            };

            // the register request is currently blocked, as the OlmAccount
            // has to be stored before we announce our keys to the Matrix server
            let mut answer;
            loop {
                answer = metaolm.receiver.recv().unwrap();
                if let Some(id) = answer.blocker_id {
                    if id == store_request_blocker {
                        break;
                    }
                }
            }

            // register request is now unblocked
            metaolm.send(register_request);
        }

        Ok(metaolm)
    }
}

impl Default for MetaolmBuilder {
    fn default() -> Self {
        Self::new()
    }
}
